.PHONY: setup
setup: ## Install all the build and lint dependencies
	go get -u github.com/alecthomas/gometalinter
	go get -u golang.org/x/tools/cmd/cover
	go get -u github.com/golang/dep/cmd/dep
	go get -u github.com/shurcooL/vfsgen/cmd/vfsgendev
	gometalinter --install --update
	@$(MAKE) dep

.PHONY: dep
dep: ## Run dep ensure and prune
	dep ensure

.PHONY: test
test: ## Run all the tests
	echo 'mode: atomic' > coverage.txt && go test -covermode=atomic -coverprofile=coverage.txt -v -race -timeout=30s ./...

.PHONY: cover
cover: test ## Run all the tests and opens the coverage report
	go tool cover -html=coverage.txt

.PHONY: fmt
fmt: ## Run goimports on all go files
	find . -name '*.go' -not -wholename './vendor/*' | while read -r file; do goimports -w "$$file"; done

.PHONY: lint
lint: ## Run all the linters
	gometalinter \
		--vendor \
		--fast \
		--enable-gc \
		--tests \
		--aggregate \
		--disable=gotype \
		--exclude=difflib.go \
		./...

.PHONY: ci
ci: lint test ## Run all the tests and code checks

.PHONY: build
build:
	go install gitlab.com/saidaspen/ispmon/cmd/ispmon

.PHONY: windows
windows:
	GOOS=windows GOARCH=386 go build gitlab.com/saidaspen/ispmon/cmd/ispmon
	GOOS=windows GOARCH=386 go build gitlab.com/saidaspen/ispmon/cmd/ispmon

.PHONY: clean
clean: ## Remove temporary files
	go clean

# Absolutely awesome: http://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
.PHONY: help
help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.DEFAULT_GOAL := build
